package com.agro.app.database.model;

import android.provider.BaseColumns;

/**
 * Created by karan316 on 12/03/16.
 */
public class Product {

    private long _id;
    private String nameEn;
    private String nameHin;
    private String imageUrl;
    private int price;
    private int isFeatured;

    public Product(){

    }

    public Product(String nameEn, String nameHin, String imageUrl, int price, int isFeatured) {
        this.nameEn = nameEn;
        this.nameHin = nameHin;
        this.imageUrl = imageUrl;
        this.price = price;
        this.isFeatured = isFeatured;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public long get_id() {
        return _id;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameHin() {
        return nameHin;
    }

    public void setNameHin(String nameHin) {
        this.nameHin = nameHin;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(int isFeatured) {
        this.isFeatured = isFeatured;
    }

    public static class ProductColumns implements BaseColumns{
        public static final String TABLE_NAME = "product";
        public static final String NAME_EN = "name_en";
        public static final String NAME_HIN = "name_hin";
        public static final String IMAGE_URL = "image_url";
        public static final String PRICE = "price";
        public static final String IS_FEATURED = "is_featured";
    }
}
