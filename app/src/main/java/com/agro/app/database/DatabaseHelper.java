package com.agro.app.database;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.agro.app.config.constants.StorageVars;
import com.agro.app.database.model.Product;
import com.agro.app.database.model.dao.ProductDao;
import com.agro.app.utils.FileUtils;
import com.agro.app.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karan316 on 12/03/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper{

    private static final String TEXT_TYPE = " TEXT ";
    private static final String COMMA_SEP = " , ";
    private static final String INT_TYPE = " INTEGER ";
    private static final String PRI_KEY = " PRIMARY KEY ";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "AgroApp.db";

    private Context mContext;
    private static DatabaseHelper mInstance;

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    public synchronized static DatabaseHelper getInstance(Context context) throws NullPointerException{
        if(mInstance==null){
            if (context == null){
                throw new NullPointerException("Context cannot be null");
            }
            mInstance = new DatabaseHelper(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("creating "," database");
        db.execSQL(getCreateQueries());
        makeDataOnCreate();
        Log.d("database", "created");
        mContext.sendBroadcast(new Intent().setAction("db_created"));
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(getDeleteQueries());
        onCreate(db);
    }

    private String getCreateQueries(){
        String createQuery = "CREATE TABLE " + Product.ProductColumns.TABLE_NAME + " (" +
                Product.ProductColumns._ID + INT_TYPE + PRI_KEY + COMMA_SEP +
                Product.ProductColumns.NAME_EN + TEXT_TYPE + COMMA_SEP +
                Product.ProductColumns.NAME_HIN + TEXT_TYPE + COMMA_SEP +
                Product.ProductColumns.IMAGE_URL + TEXT_TYPE + COMMA_SEP +
                Product.ProductColumns.PRICE + INT_TYPE + COMMA_SEP +
                Product.ProductColumns.IS_FEATURED + INT_TYPE +
        " )";

        return createQuery;
    }

    private String getDeleteQueries(){
        String deleteQuery = "DROP TABLE IF EXISTS " + Product.ProductColumns.TABLE_NAME;
        return deleteQuery;
    }

    private void makeDataOnCreate(){
        try {
            JSONArray array = new JSONArray(FileUtils.getJSONFromAssetsFile(mContext));
            ProductDao productDao = new ProductDao(mInstance);
            for (int i=0;i<array.length();i++){
                JSONObject object = array.getJSONObject(i);
                Product product = new Product();
                product.setIsFeatured(object.getInt("isFeatured"));
                product.setPrice(object.getInt("price"));
                product.setImageUrl(object.getString("imgUrl"));
                product.setNameEn(object.getString("nameEn"));
                product.setNameHin(object.getString("nameHin"));
                productDao.insert(product, null);
                Log.d("inserted", " " + i);
                object=null;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
