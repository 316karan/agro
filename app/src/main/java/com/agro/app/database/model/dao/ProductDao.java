package com.agro.app.database.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.agro.app.DBCallback;
import com.agro.app.database.DatabaseHelper;
import com.agro.app.database.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karan316 on 12/03/16.
 */
public class ProductDao {

    private DatabaseHelper mDBHelper;

    public ProductDao(DatabaseHelper mDBHelper) {
        this.mDBHelper = mDBHelper;
    }

    private long insert(Product product){
        ContentValues contentValues = new ContentValues();
        contentValues.put(Product.ProductColumns.IS_FEATURED,product.getIsFeatured());
        contentValues.put(Product.ProductColumns.IMAGE_URL,product.getImageUrl());
        contentValues.put(Product.ProductColumns.NAME_EN,product.getNameEn());
        contentValues.put(Product.ProductColumns.NAME_HIN,product.getNameHin());
        contentValues.put(Product.ProductColumns.PRICE, product.getPrice());
        return mDBHelper.getWritableDatabase()
                    .insert(Product.ProductColumns.TABLE_NAME,
                            null,
                            contentValues);
    }

    public void insert(final Product product, final DBCallback<Long> dbCallback){
        new AsyncTask<Void,Void,Long>(){
            @Override
            protected Long doInBackground(Void... voids) {
                return insert(product);
            }

            @Override
            protected void onPostExecute(Long id) {
                if (dbCallback!=null) {
                    if (id!=null) {
                        dbCallback.onResponse(id);
                    } else {
                        dbCallback.onFail("Could not insert product");
                    }
                }
            }
        }.execute();
    }

    private List<Product> findAll(){
        List<Product> products = new ArrayList<>();

        String[] projections = new String[]{
                Product.ProductColumns._ID,
                Product.ProductColumns.NAME_EN,
                Product.ProductColumns.NAME_HIN,
                Product.ProductColumns.IMAGE_URL,
                Product.ProductColumns.PRICE,
                Product.ProductColumns.IS_FEATURED
        };

        Cursor c = mDBHelper.getReadableDatabase().query(
                Product.ProductColumns.TABLE_NAME,
                projections,
                null,
                null,
                null,
                null,
                null
        );

        if(c.moveToFirst()){
            do {
                products.add(getProductFromCursor(c));
            }while (c.moveToNext());
        }
        c.close();
        return products;
    }

    private Product getProductFromCursor(Cursor cursor){
        Product product = new Product();
        product.set_id(cursor.getLong(cursor.getColumnIndexOrThrow(Product.ProductColumns._ID)));
        product.setNameEn(cursor.getString(cursor.getColumnIndexOrThrow(Product.ProductColumns.NAME_EN)));
        product.setNameHin(cursor.getString(cursor.getColumnIndexOrThrow(Product.ProductColumns.NAME_HIN)));
        product.setImageUrl(cursor.getString(cursor.getColumnIndexOrThrow(Product.ProductColumns.IMAGE_URL)));
        product.setPrice(cursor.getInt(cursor.getColumnIndexOrThrow(Product.ProductColumns.PRICE)));
        product.setIsFeatured(cursor.getInt(cursor.getColumnIndexOrThrow(Product.ProductColumns.IS_FEATURED)));
        return product;
    }

    public void findAll(final DBCallback<List<Product>> dbCallback){
        new AsyncTask<Void,Void,List<Product>>(){
            @Override
            protected List<Product> doInBackground(Void... params) {
                return findAll();
            }

            @Override
            protected void onPostExecute(List<Product> products) {
                super.onPostExecute(products);
                dbCallback.onResponse(products);
            }
        }.execute();
    }

    private List<Product> search(String searchQuery){
        List<Product> products = new ArrayList<>();

        String[] projections = new String[]{
                Product.ProductColumns._ID,
                Product.ProductColumns.NAME_EN,
                Product.ProductColumns.NAME_HIN,
                Product.ProductColumns.IMAGE_URL,
                Product.ProductColumns.PRICE,
                Product.ProductColumns.IS_FEATURED
        };

        String selection = Product.ProductColumns.NAME_EN + " LIKE '%" + searchQuery + "%'";

        Cursor c = mDBHelper.getReadableDatabase().query(
                Product.ProductColumns.TABLE_NAME,
                projections,
                selection,
                null,
                null,
                null,
                null
        );

        if(c.moveToFirst()){
            do {
                products.add(getProductFromCursor(c));
            }while (c.moveToNext());
        }
        c.close();
        return products;
    }

    public void search(final String searchQuery,final DBCallback<List<Product>> dbCallback){
        new AsyncTask<Void,Void,List<Product>>(){
            @Override
            protected List<Product> doInBackground(Void... params) {
                return search(searchQuery);
            }

            @Override
            protected void onPostExecute(List<Product> products) {
                super.onPostExecute(products);
                dbCallback.onResponse(products);
            }
        }.execute();
    }

}
