package com.agro.app;

/**
 * Created by karan316 on 12/03/16.
 */
public interface DBCallback<T> {

    void onResponse(T response);
    void onFail(String reason);

}
