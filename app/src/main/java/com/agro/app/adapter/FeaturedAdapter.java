package com.agro.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.agro.app.database.model.Product;
import com.agro.app.fragment.FeaturedFragment;

import java.util.List;

/**
 * Created by karan316 on 12/03/16.
 */
public class FeaturedAdapter extends FragmentPagerAdapter {

    private List<Product> mProducts;

    public FeaturedAdapter(FragmentManager fm,List<Product> products) {
        super(fm);
        if (fm.getFragments() != null) {
            fm.getFragments().clear();
        }
        this.mProducts = products;
    }

    @Override
    public Fragment getItem(int position) {
        return FeaturedFragment.newInstance(mProducts.get(position).getImageUrl());
    }

    @Override
    public int getCount() {
        return mProducts.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }


}
