package com.agro.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agro.app.R;
import com.agro.app.config.constants.StaticVars;
import com.agro.app.config.constants.StorageVars;
import com.agro.app.database.model.Product;
import com.agro.app.utils.SessionManager;
import com.agro.app.utils.imageloader.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by karan316 on 12/03/16.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private List<Product> mProducts;
    private Context mContext;
    private ImageLoader mImageLoader;

    public ProductAdapter(List<Product> mProducts,Context mContext) {
        this.mProducts = mProducts;
        this.mContext = mContext;
        mImageLoader = new ImageLoader(mContext);
    }

    public void setProduct(List<Product> mProducts){
        this.mProducts = mProducts;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(SessionManager.getInstance(mContext).getStringPref(StorageVars._preferredLanguage).equals("") || SessionManager.getInstance(mContext).getStringPref(StorageVars._preferredLanguage).equalsIgnoreCase(StaticVars._languageEn)){
            holder.tvName.setText(mProducts.get(position).getNameEn());
        }else {
            holder.tvName.setText(mProducts.get(position).getNameHin());
        }
        holder.tvPrice.setText("\u20B9 "+mProducts.get(position).getPrice());
//        Picasso.with(mContext).load(mProducts.get(position).getImageUrl()).into(holder.ivImage);
        mImageLoader.DisplayImage(mProducts.get(position).getImageUrl(),R.drawable.loading,holder.ivImage);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, null);
        ViewHolder v = new ViewHolder(layoutView);
        return v;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvPrice,tvName;
        public ImageView ivImage;

        public ViewHolder(View itemView) {
            super(itemView);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_product_price);
            tvName = (TextView) itemView.findViewById(R.id.tv_product_name);
            ivImage = (ImageView) itemView.findViewById(R.id.iv_product_image);
        }
    }
}
