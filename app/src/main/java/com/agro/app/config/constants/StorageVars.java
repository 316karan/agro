package com.agro.app.config.constants;

/**
 * Created by karan on 12/3/15.
 */
public class StorageVars {

    private static final String _prefix = "com.agro.app";

    public static final String _preferredLanguage = _prefix + "preferred_language";

}
