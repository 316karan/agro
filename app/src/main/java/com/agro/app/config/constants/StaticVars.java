package com.agro.app.config.constants;

/**
 * Created by karan on 6/12/15.
 */
public class StaticVars {

    /**
     * shared pref
     * */
    public static final String _preferenceName = "app-agro-pref";

    public static final String _languageEn = "en";
    public static final String _languageHin = "hin";

}
