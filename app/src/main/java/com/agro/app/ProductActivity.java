package com.agro.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.agro.app.adapter.FeaturedAdapter;
import com.agro.app.adapter.ProductAdapter;
import com.agro.app.config.constants.StaticVars;
import com.agro.app.config.constants.StorageVars;
import com.agro.app.database.DatabaseHelper;
import com.agro.app.database.model.Product;
import com.agro.app.database.model.dao.ProductDao;
import com.agro.app.utils.SessionManager;
import com.agro.app.utils.imageloader.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ProductActivity extends AppCompatActivity implements View.OnClickListener{

    private DatabaseHelper mDbHelper;
    private ProductDao mProductDao;
    private RecyclerView mProductList;
    private ProductAdapter mProductAdapter;
    private List<Product> mProducts;
    private EditText mEtSearch;
    private ImageView mIvSearch;
    private ViewPager mPager;
    private ImageView mIvSwap;
    private FeaturedAdapter mFeaturedAdapter;

    private boolean isSearchedView;
    private int mCurrentPage;
    private ImageView mIvLeft,mIvRight;
    private Timer mTimer;


    private BroadcastReceiver mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mProductList = (RecyclerView)findViewById(R.id.product_list);
        mEtSearch = (EditText) findViewById(R.id.et_search);
        mIvSearch = (ImageView) findViewById(R.id.btn_search);
        mIvSearch.setOnClickListener(this);
        mPager = (ViewPager) findViewById(R.id.img_pager);
        mIvSwap = (ImageView) findViewById(R.id.btn_swap);
        mIvLeft = (ImageView) findViewById(R.id.iv_left);
        mIvRight = (ImageView) findViewById(R.id.iv_right);
        mIvSwap.setOnClickListener(this);
        mIvRight.setOnClickListener(this);
        mIvLeft.setOnClickListener(this);

        GridLayoutManager lLayout = new GridLayoutManager(this, 2);
        mProductList.setLayoutManager(lLayout);

        mDbHelper = DatabaseHelper.getInstance(this);
        mProductDao = new ProductDao(mDbHelper);

        getAllProducts();

        IntentFilter filter = new IntentFilter();
        filter.addAction("db_created");

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("db","created");
                getAllProducts();
            }
        };

        registerReceiver(mReceiver,filter);

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private void getAllProducts(){
        mProductDao.findAll(new DBCallback<List<Product>>() {
            @Override
            public void onResponse(List<Product> response) {
                mProducts = response;
                mFeaturedAdapter = new FeaturedAdapter(getSupportFragmentManager(),getFeaturedProducts());
                mPager.setAdapter(mFeaturedAdapter);
                manipulateFeaturedPager();
                enableDisableCarouselKeys();
                configureTimer();
                if (mProductAdapter==null) {
                    mProductAdapter = new ProductAdapter(mProducts, ProductActivity.this);
                    mProductList.setAdapter(mProductAdapter);
                }else
                    mProductAdapter.setProduct(mProducts);
                isSearchedView=false;
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    private void manipulateFeaturedPager(){
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d("page", "scrolled");
            }

            @Override
            public void onPageSelected(int position) {
                Log.d("current page", position + "");
                mCurrentPage = position;
                enableDisableCarouselKeys();
                configureTimer();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.d("page", "scrolled state changed");
            }
        });
    }

    private void enableDisableCarouselKeys(){
        if(mCurrentPage==0) {
            mIvLeft.setVisibility(View.GONE);
        }else {
            mIvLeft.setVisibility(View.VISIBLE);
        }
        if(mCurrentPage+1 == mFeaturedAdapter.getCount()){
            mIvRight.setVisibility(View.GONE);
        }else {
            mIvRight.setVisibility(View.VISIBLE);
        }
    }

    private void configureTimer(){
        stopTimer();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        movePagerRight();
                    }
                });
            }
        },3000,3000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        configureTimer();
    }

    private void stopTimer(){
        if(mTimer!=null){
            mTimer.cancel();
            mTimer = null;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_search:
                mProductDao.search(mEtSearch.getText().toString().trim(), new DBCallback<List<Product>>() {
                    @Override
                    public void onResponse(List<Product> response) {
                        mProducts = response;
                        mProductAdapter.setProduct(mProducts);
                        isSearchedView = true;
                    }

                    @Override
                    public void onFail(String reason) {

                    }
                });
                break;
            case R.id.btn_swap:
                if(SessionManager.getInstance(this).getStringPref(StorageVars._preferredLanguage).equals(StaticVars._languageEn))
                    SessionManager.getInstance(this).insertStringPref(StorageVars._preferredLanguage, StaticVars._languageHin);
                else
                    SessionManager.getInstance(this).insertStringPref(StorageVars._preferredLanguage, StaticVars._languageEn);
                mProductAdapter.notifyDataSetChanged();
                break;
            case R.id.iv_left:
                if(mCurrentPage>0){
                    mPager.setCurrentItem(mCurrentPage-1);
                }
                break;
            case R.id.iv_right:
                movePagerRight();
                break;
        }
    }

    private void movePagerRight(){
        if(mCurrentPage<mFeaturedAdapter.getCount()-1){
            mPager.setCurrentItem(mCurrentPage+1);
        }else {
            mPager.setCurrentItem(0);
        }
    }

    private List<Product> getFeaturedProducts(){
        List<Product> products = new ArrayList<>();
        for(Product product : mProducts){
            if(product.getIsFeatured()==1){
                products.add(product);
            }
        }
        return products;
    }

    @Override
    public void onBackPressed() {
        if(isSearchedView){
            getAllProducts();
            return;
        }
        super.onBackPressed();
    }

}
