package com.agro.app.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by karan316 on 12/03/16.
 */
public class FileUtils {

    public static String getJSONFromAssetsFile(Context context){
        String json = null;
        try {
            InputStream is = context.getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
