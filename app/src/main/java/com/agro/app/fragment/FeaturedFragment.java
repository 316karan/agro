package com.agro.app.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.agro.app.R;
import com.agro.app.utils.imageloader.ImageLoader;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeaturedFragment extends Fragment {

    private ImageView mImg;
    private String mImgUrl;
    private ImageLoader mImageLoader;

    public FeaturedFragment() {
        // Required empty public constructor
    }

    public static FeaturedFragment newInstance(String url){
        Bundle bundle = new Bundle();
        bundle.putString("img", url);
        FeaturedFragment featuredFragment = new FeaturedFragment();
        featuredFragment.setArguments(bundle);
        return featuredFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null)
            mImgUrl = getArguments().getString("img");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mImageLoader = new ImageLoader(getActivity());
        return inflater.inflate(R.layout.fragment_featured, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mImg = (ImageView) getView().findViewById(R.id.img_fragment_pager);
//        Picasso.with(getActivity()).load(mImgUrl).into(mImg);
        mImageLoader.DisplayImage(mImgUrl,R.drawable.loading,mImg);
    }
}
